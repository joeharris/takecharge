# README #

* Python CGI website using Bootstrap built during a one-day hackathon. A demo site that won first place in the CSIRO Solar Data Hackathon 2016. A platform for comparing households (on a novel metric) on their energy habits and allowing them to compete against each other.